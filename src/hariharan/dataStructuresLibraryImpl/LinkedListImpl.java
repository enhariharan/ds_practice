package hariharan.dataStructuresLibraryImpl;

import hariharan.dataStructuresLibrary.LinkedList;

public class LinkedListImpl<T extends Comparable<? super T>> implements LinkedList<T> {
    
    public LinkedListImpl(T value) {
        Node node = new Node(value);
        if (_head == null) {
            _head = node;
            _tail = _head;
        }
    }
    
    public LinkedListImpl() {
        _head = null;
        _tail = null;
    }
    
    @Override
    public int countOccurence(T data) {
        int count = 0;
        for (Node temp = _head; temp != null; temp = temp._next) {
            if (temp._value == data) {
                ++count;
            } // if (temp._data == data)
        } // for (Vertex temp = _head; temp != null; temp = temp._next)
        return count;
    } // countOccurence()
    
    @Override
    public T getNth(int index) throws IndexOutOfBoundsException {
        if (index < 0) {
            throw new IndexOutOfBoundsException();
        }
        T data;
        Node temp = _head;
        int count = 0;
        for (; temp != null && count < index; temp = temp._next, count++)
            ;
        if (temp != null) {
            data = temp._value;
        } else {
            throw new IndexOutOfBoundsException();
        }
        return data;
    }
    
    @Override
    public LinkedList<T> AlternatingMerge(LinkedList<T> list2) {
        if (list2 != null) {
            Node temp1 = _head;
            Node temp1Next = temp1._next;
            LinkedListImpl<T> tempList = (LinkedListImpl<T>) list2;
            Node temp2 = tempList._head;
            Node temp2Next = temp2._next;
            
            do {
                if (temp1 != null && temp2 != null)
                    temp1._next = temp2;
                if (temp2 != null && temp1Next != null)
                    temp2._next = temp1Next;
                temp1 = temp1Next;
                temp2 = temp2Next;
                if (temp1Next != null)
                    temp1Next = temp1Next._next;
                if (temp2Next != null)
                    temp2Next = temp2Next._next;
            } while ((temp1 != null) || (temp2 != null));
        }
        return this;
    }
    
    @Override
    public LinkedList<T>[] AlternatingSplit() {
        @SuppressWarnings("unchecked")
        LinkedListImpl<T>[] lists = new LinkedListImpl[2];
        lists[0] = this;
        lists[1] = null;
        if (_head._next == null) {
            // list has only one node, so return now
            return lists;
        }
        
        Node temp1 = _head;
        Node temp2 = _head._next;
        
        lists[1] = new LinkedListImpl<T>();
        lists[1]._head = temp2;
        
        do {
            if (temp1 != null && temp2 != null) {
                temp1._next = temp2._next;
                temp1 = temp1._next;
            }
            if (temp1 != null && temp2 != null) {
                temp2._next = temp1._next;
                temp2 = temp2._next;
            }
            
        } while (temp1 != null && temp2 != null);
        
        return lists;
    }
    
    @Override
    public void insertAt(int index, T data) throws IndexOutOfBoundsException {
        if ((index < 0) || (index > size())) {
            throw new IndexOutOfBoundsException();
        }
        
        Node node = new Node(data);
        
        if (index == 0) {
            // insert at beginning
            node._next = _head;
            _head = node;
        } else if (index == size()) {
            // insert at end
            _tail._next = node;
            _tail = node;
        } else {
            // insert in middle
            Node temp = _head;
            for (int n = 0; temp._next != null; temp = temp._next) {
                if (++n == index) {
                    break;
                }
            }
            node._next = temp._next;
            temp._next = node;
        }
    }
    
    @Override
    public void push(T data) {
        insertAt(0, data);
    }
    
    @Override
    public LinkedList<T> append(T data) {
        Node node = new Node(data);
        if (_head == null) {
            _head = node;
            _tail = _head;
        } else {
            _tail._next = node;
            _tail = _tail._next;
        }
        return this;
    }
    
    @Override
    public LinkedList<T> insertSorted(T data) {
        Node current = _head;
        Node prev = null;
        Node newNode = new Node(data);
        boolean isInserted = false;
        do {
            if (current.isGreaterThanOrEqualTo(data)) {
                newNode._next = current;
                if (current == _head) {
                    // insert at beginning
                    _head = newNode;
                    isInserted = true;
                } else {
                    // insert in middle
                    prev._next = newNode;
                    isInserted = true;
                }
            }
            prev = current;
            current = current._next;
        } while ((isInserted == false) && (current != null));
        if ((isInserted == false) && (current == null)) {
            // insert at end
            prev._next = newNode;
            isInserted = true;
        }
        return this;
    }
    
    public void mergeSort() {
        if (_head == null)
            return;
        
        if (_head._next == null)
            return;
        
        LinkedListImpl<T>[] lists = (LinkedListImpl<T>[]) this.split(2);
        lists[0].mergeSort();
        lists[1].mergeSort();
        lists[0].sortedMerge(lists[1]);
        _head = lists[0]._head;
    }
    
    @Override
    public void moveNode(LinkedList<T> inputList) throws NullPointerException {
        if (inputList == null)
            throw new NullPointerException();
        
        Node node = new Node(inputList.getNth(0));
        if (_head != null) {
            node._next = _head;
        }
        _head = node;
        inputList.removeNode(0);
    }
    
    @Override
    public void purge() {
        Node temp = _head;
        do {
            _head = temp._next;
            temp._next = null;
            temp = _head;
        } while (temp != null);
        _tail = null;
    }
    
    @Override
    public void remove(T data) {
        Node current = _head;
        Node prev = null;
        
        while (current != null) {
            if (current._value != data) {
                // This node is not to be deleted, move on to next node
                prev = current;
                current = current._next;
            } else {
                // This node must be deleted
                if (prev == null) {
                    // head node is to be deleted
                    _head = current._next;
                    current = _head;
                } else {
                    // Middle node is being deleted
                    if (current._next != null && current._next._value != data) {
                        prev._next = current._next;
                        prev = current;
                    }
                    current = current._next;
                } // if (prev == null) ... else
            } // if (current._data != data) ... else
        } // while (current != null)
        if (prev != null) {
            prev._next = null;
        }
    } // remove()
    
    @Override
    public void removeNode(int index) throws IndexOutOfBoundsException {
        if (index < 0) {
            throw new IndexOutOfBoundsException();
        }
        
        Node current = _head;
        Node prev = null;
        
        for (int i = 0; ((current != null) && (i < index)); ++i, prev = current, current = current._next)
            ;
        if (current == null) {
            throw new IndexOutOfBoundsException();
        } else if (current == _head) {
            _head = _head._next;
            current._next = null;
        } else {
            prev._next = current._next;
            current._next = null;
        }
    }
    
    @Override
    public void reverse() {
        if (_head._next == null) {
            return;
        }
        Node head = _head; // points to root node
        Node current = head._next; // points to the next node
        Node next = current._next; // points to the next-to-next node
        
        head._next = null; // initialize the last node
        
        while (current != null) {
            current._next = head;
            head = current;
            current = next;
            if (next != null) {
                next = next._next;
            } // if (temp2._next != null)
        } // for (int size = size(); size > 0; size--)
        _head = head;
    } // reverse()
    
    public void recursiveReverse() {
        recursiveReverse(_head);
    }
    
    private void recursiveReverse(Node headref) {
        Node first;
        Node rest;
        
        if (headref == null) {
            return;
        }
        first = headref;
        rest = first._next;
        
        if (rest == null) {
            return;
        }
        
        recursiveReverse(rest);
        
        first._next._next = first;
        first._next = null;
        _head = rest;
    }
    
    @Override
    public void setAt(int index, T data) throws IndexOutOfBoundsException {
        if (index < 0) { // invalid index
            throw new IndexOutOfBoundsException();
        }
        
        Node temp = _head;
        for (int n = 0; n < index && temp != null; n++, temp = temp._next)
            ;
        
        if (temp == null) { // invalid index
            throw new IndexOutOfBoundsException();
        } else { // valid index and node found, so go ahead and change the data
            temp._value = data;
        }
    }
    
    @Override
    public int size() {
        int size = 0;
        for (Node temp = _head; temp != null; size++)
            temp = temp._next;
        return size;
    }
    
    /**
     * Given two sorted lists list1 and list 2, this method returns a new list
     * which contains the common elements in list1 and list2.
     */
    public LinkedList<T> sortedListIntersection(LinkedList<T> list2) {
        if (list2 == null) {
            return null;
        }
        LinkedListImpl<T> result = null;
        Node current1 = _head;
        LinkedListImpl<T> List2 = (LinkedListImpl<T>) list2;
        Node current2 = List2._head;
        
        while (current1 != null && current2 != null) {
            if (true == current1.isLesserThan(current2)) {
                current1 = current1._next;
            } else if (true == current1.isGreaterThan(current2)) {
                current2 = current2._next;
            } else {
                if (result == null) {
                    result = new LinkedListImpl<T>(current1._value);
                } else {
                    result.append(current1._value);
                } // if (result == null) ... else
                current1 = current1._next;
                current2 = current2._next;
            } // if (current1._data < current2._data) ... else if
              // (current1._data > current2._data) ... else
        } // while (current1 != null && current2 != null)
        
        return result;
    }
    
    /**
     * Merges a sorted list list2 into this list. This method assumes that this
     * list and list2 are both sorted lists. The resulting merged list is
     * returned.
     */
    public void sortedMerge(/* LinkedList list1, */LinkedList<T> list2) {
        if (list2 == null) {
            return;
        }
        LinkedListImpl<T> List1 = this;
        LinkedListImpl<T> List2 = (LinkedListImpl<T>) list2;
        Node head1 = _head;
        Node head2 = List2._head;
        if (head1 != null && head2 != null) {
            if (head1._next == null) {
                List1 = (LinkedListImpl<T>) List2.insertSorted(List1.getNth(0));
                _head = List2._head;
            } else if (head2._next == null) {
                List1 = (LinkedListImpl<T>) List1.insertSorted(List2.getNth(0));
            } else {
                Node current1 = head1;
                Node next1 = null;
                if (current1 != null) {
                    next1 = current1._next;
                }
                
                Node current2 = head2;
                Node next2 = null;
                if (current2 != null) {
                    next2 = current2._next;
                }
                
                while ((current1 != null) && (current2 != null)) {
                    if (current2.isGreaterThanOrEqualTo(current1._value)) {
                        while ((next1 != null) && (current2 != null) && (next1.isLesserThanOrEqualTo(current2._value))) {
                            current1 = next1;
                            next1 = next1._next;
                        }
                        current1._next = current2;
                        current1 = next1;
                        if (next1 != null) {
                            next1 = next1._next;
                        }
                    } else {
                        while ((next2 != null) && (current1 != null) && (next2.isLesserThanOrEqualTo(current1))) {
                            current2 = next2;
                            next2 = next2._next;
                        }
                        current2._next = current1;
                        current2 = next2;
                        if (next2 != null) {
                            next2 = next2._next;
                        }
                        if (head1 == current1) {
                            head1 = head2;
                            _head = head1;
                        } // if (head1 == current1)
                    } // if (current2._data >= current1._data) ... else
                } // while ((current1 != null) && (current2 != null))
            } // if (head1.size() == 1) ... else if (head2.size() == 1) ... else
        } // if (head1 != null && head2 != null)
    } // sortedMerge()
    
    @Override
    public LinkedList<T>[] split(int numberOfSplits) throws IndexOutOfBoundsException {
        if (numberOfSplits > this.size()) {
            throw new IndexOutOfBoundsException("Incorrect input value");
        }
        @SuppressWarnings("unchecked")
        LinkedListImpl<T>[] lists = new LinkedListImpl[numberOfSplits];
        Node head = _head;
        Node prev = null;
        Node current = head;
        Node fastMovingPointer = head;
        for (int i = 0, j = numberOfSplits; j > 0; --j, ++i) {
            do {
                prev = current;
                current = current._next;
                for (int k = 0; k < j; k++) {
                    if (fastMovingPointer != null)
                        fastMovingPointer = fastMovingPointer._next;
                }
            } while (fastMovingPointer != null);
            lists[i] = new LinkedListImpl<T>();
            lists[i]._head = head;
            head = current;
            prev._next = null;
            prev = null;
            fastMovingPointer = head;
        }
        return lists;
    }
    
    @Override
    public String toString() {
        return "<LinkedListImpl>: [ " + _head + " ] </LinkedListImpl>";
    }
    
    private Node _head;
    private Node _tail;
    
    private class Node {
        T    _value; // Pay-load of this node
        Node _next; // Next item in the linked list
                     
        Node(T value) {
            _value = value;
            _next = null;
        }
        
        public boolean isLesserThanOrEqualTo(Node current) {
            return isLesserThanOrEqualTo(current._value);
        }
        
        private boolean isLesserThanOrEqualTo(T data) {
            return (isLesserThan(data) || isEqualTo(data));
        }
        
        @SuppressWarnings("unused")
        public boolean isGreaterThanOrEqualTo(Node current) {
            return isGreaterThanOrEqualTo(current._value);
        }
        
        public boolean isGreaterThanOrEqualTo(T data) {
            return (isGreaterThan(data) || isEqualTo(data));
        }
        
        public boolean isGreaterThan(T data) {
            return (_value.compareTo(data) > 0);
        }
        
        public boolean isEqualTo(T data) {
            return (_value.compareTo(data) == 0);
        }
        
        @SuppressWarnings("unused")
        boolean isEqualTo(Node node) {
            return isEqualTo(node._value);
        }
        
        boolean isGreaterThan(Node node) {
            return (_value.compareTo(node._value) > 0);
        }
        
        boolean isLesserThan(Node node) {
            return isLesserThan(node._value);
        }
        
        public boolean isLesserThan(T data) {
            return (_value.compareTo(data) < 0);
        }
        
        @Override
        public String toString() {
            String returnVal = "<Node>";
            Node temp = this;
            while (temp != null) {
                returnVal += temp._value + " --> ";
                temp = temp._next;
            } // while (temp != null)
            return returnVal + "/ </Node>";
        } // toString()
    } // class Node
} // class LinkedListImpl