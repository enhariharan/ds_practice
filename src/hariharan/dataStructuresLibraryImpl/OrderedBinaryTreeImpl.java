/**
 * 
 */
package hariharan.dataStructuresLibraryImpl;

import hariharan.dataStructuresLibrary.OrderedBinaryTree;

/**
 * @author hariharan
 * 
 */
public class OrderedBinaryTreeImpl<T extends Comparable<? super T>> implements OrderedBinaryTree<T> {
    public OrderedBinaryTreeImpl() {
        _root = null;
    }
    
    public OrderedBinaryTreeImpl(T data) {
        this();
        Node node = new Node(data);
        _root = node;
    }
    
    @Override
    public String toString() {
        return "Tree ***** " + _root + " *****";
    }
    
    private Node _root;
    
    private class Node {
        Node() {
            this(null, null);
        }
        
        Node(T data) {
            this();
            this._data = data;
        }
        
        private Node(Node left, Node right) {
            this._left = left;
            this._right = right;
        }
        
        public boolean isGreaterThan(T data) {
            return (this._data.compareTo(data) > 0);
        }
        
        public boolean isLesserThan(T data) {
            return (this._data.compareTo(data) < 0);
        }
        
        @Override
        public String toString() {
            String result = "[ ";
            result = result + this._left + " <-- | ";
            result = result + this._data + " | ";
            result = result + " --> " + this._right;
            result = result + " ]";
            return result;
        }
        
        T    _data;
        Node _left;
        Node _right;
    }
    
    @Override
    public OrderedBinaryTree<T> insert(T data) {
        _root = insert(_root, data);
        return this;
    }
    
    private Node insert(Node root, T data) {
        if (root == null) {
            root = new Node(data);
        } else if (root.isGreaterThan(data)) {
            root._left = insert(root._left, data);
        } else {
            root._right = insert(root._right, data);
        }
        
        return root;
    }
    
    @Override
    public T getRoot() {
        T returnVal;
        if (this._root == null) {
            returnVal = null;
        } else {
            returnVal = this._root._data;
        }
        return returnVal;
    }
    
    @Override
    public boolean isPresent(T data) {
        boolean result = isPresent(_root, data);
        return result;
    }
    
    private boolean isPresent(Node root, T data) {
        boolean result = false;
        if (root != null) {
            if (root.isGreaterThan(data)) {
                result = isPresent(root._left, data);
            } else if (root.isLesserThan(data)) {
                result = isPresent(root._right, data);
            } else {
                result = true;
            }
        }
        return result;
    }
    
    @Override
    public int size() {
        return size(_root);
    }
    
    private int size(Node root) {
        if (root == null) {
            return 0;
        } else if ((root._left == null) && (root._right == null)) {
            return 1;
        } else {
            return (size(root._left) + size(root._right) + 1);
        }
    }
    
    @Override
    public int maxDepth() {
        int maxDepth = maxDepth(_root) - 1;
        if (maxDepth < 0) {
            maxDepth = 0;
        }
        return maxDepth;
    }
    
    private int maxDepth(Node root) {
        if (root == null) {
            return 0;
        } else {
            int lDepth = maxDepth(root._left);
            int rDepth = maxDepth(root._right);
            
            if (lDepth > rDepth) {
                return lDepth + 1;
            } else {
                return rDepth + 1;
            } // if (lDepth > rDepth) ... else
        } // if (root == null) ... else
    } // maxDepth()
    
    @Override
    public T minValue() {
        return minValue(_root);
    }
    
    public T minValue(Node root) {
        T minValue = null;
        if (root != null) {
            if ((root._left == null) && (root._right == null)) {
                minValue = root._data;
            } else {
                Node node = root;
                while (node._left != null) {
                    node = node._left;
                } // while (node._left != null)
                minValue = node._data;
            } // if ((root._left == null) && (root._right == null)) ... else
        } // if (root != null)
        return minValue;
    } // public int minValue(Vertex root)
    
    @Override
    public String printInorder() {
        return printInorder(_root);
    }
    
    public String printInorder(Node root) {
        String result = null;
        if (root == null) {
            return null;
        }
        
        String temp = printInorder(root._left);
        if (temp != null) {
            result = temp;
        }
        
        temp += root._data; // new Integer(root._data).toString();
        if (temp != null) {
            if (result == null) {
                result = temp;
            } else {
                result += " --> " + temp;
            }
        }
        
        temp = printInorder(root._right);
        if (temp != null) {
            result += " --> " + temp;
        }
        
        return result;
    }
    
    @Override
    public String printPostorder() {
        return printPostorder(_root);
    }
    
    public String printPostorder(Node root) {
        String result = null;
        if (root == null) {
            return null;
        }
        
        String temp = printPostorder(root._left);
        if (temp != null) {
            result = temp;
        }
        
        temp = printPostorder(root._right);
        if (temp != null) {
            if (result == null) {
                result = temp;
            } else {
                result += " --> " + temp;
            }
        }
        
        temp += root._data;
        if (temp != null) {
            if (result == null) {
                result = temp;
            } else {
                result += " --> " + temp;
            }
        }
        
        return result;
    }
    
    @Override
    public void printPaths() {
        String path = "";
        printPaths(_root, path);
        return;
    }
    
    private void printPaths(Node root, String path) {
        Boolean isEmptySubtree = (root == null);
        Boolean isLeafNode = (isEmptySubtree == false) && ((root._left == null) && (root._right == null));
        if ((isEmptySubtree == true) || (isLeafNode == true)) {
            if (isEmptySubtree == true)
                path += "/";
            if (isLeafNode == true)
                path += root._data + "/";
            System.out.println(path);
            return;
        } else {
            path += root._data + " ";
            if (root._left != null) {
                printPaths(root._left, path);
            }
            if (root._right != null) {
                printPaths(root._right, path);
            }
            return;
            // } // if ((root._left == null) && (root._right == null)) ... else
        } // if (root == null) ... else
    } // printPaths()
    
    @Override
    public void mirror() {
        mirror(_root);
        return;
    }
    
    private void mirror(Node root) {
        if (root != null) {
            if ((root._left == null) && (root._right == null)) {
                return;
            } else {
                Node temp = root._left;
                root._left = root._right;
                root._right = temp;
                
                mirror(root._left);
                mirror(root._right);
            } // if ((root._left == null) && (root._right == null)) ... else
        } // if (root != null)
        return;
    }
    
    private Node getRootNode() {
        return this._root;
    }
    
    public boolean isSameTree(Node root1, Node root2) {
        Boolean flag = false;
        if ((root1 == null) && (root2 == null)) {
            return true;
        } else if ((root1 != null) && (root2 != null)) {
            flag = (root1._data == root2._data);
            if (flag != false) {
                flag = isSameTree(root1._left, root2._left);
                if (flag != false) {
                    flag = isSameTree(root1._left, root2._left);
                }
            } // if (flag != false)
        } // if ((root1 == null) && (root2 == null)) ... else if ((root1 !=
          // null) && (root2 != null))
        return flag;
    }
    
    @Override
    public int countBinarySearchTrees(int nodesCount) {
        int count = 0;
        
        if (nodesCount <= 1) {
            count += 1;
        } else {
            for (int i = 1; i <= nodesCount; i++) {
                int countLeftBSTs = countBinarySearchTrees(i - 1);
                int countRightBSTs = countBinarySearchTrees(nodesCount - i);
                count += countLeftBSTs * countRightBSTs;
            }
        }
        return count;
    }
    
    @Override
    public CircularDoubleLinkedList<T> treeToDoublyLinkedList() {
        CircularDoubleLinkedList<T> list = new CircularDoubleLinkedList<T>();
        return treeToDoublyLinkedList(this._root, list);
    }
    
    private CircularDoubleLinkedList<T> treeToDoublyLinkedList(Node root, CircularDoubleLinkedList<T> list) {
        if (root != null) {
            list.append(root._data);
            if ((root._left != null) || (root._right != null)) {
                list = treeToDoublyLinkedList(root._left, list);
                list = treeToDoublyLinkedList(root._right, list);
            } // if ((root._left != null) || (root._right != null)) ... else
        } // if (root != null)
        
        return list;
    } // treeToDoublyLinkedList()
    
    @Override
    public boolean isSameTree(OrderedBinaryTree<T> tree2) {
        OrderedBinaryTreeImpl<T> tree = (OrderedBinaryTreeImpl<T>) tree2;
        if ((this._root == null) || (tree == null))
            return false;
        return isSameTree(this._root, tree.getRootNode());
    }
} // class OrderedBinaryTreeImpl