/**
 * 
 */
package hariharan.dataStructuresLibraryImpl;

import hariharan.dataStructuresLibrary.LinkedList;

/**
 * @author hariharan
 * 
 */
public class CircularDoubleLinkedList<T extends Comparable<? super T>> implements LinkedList<T> {
    
    /**
     * @param _value
     */
    @Override
    public CircularDoubleLinkedList<T> append(T data) {
        Node node = new Node(data);
        if (this._head == null) {
            this._head = node;
        } else {
            Node nextNode = this._head;
            Node prevNode = nextNode._prev;
            if (prevNode == null)
                prevNode = nextNode;
            
            node._next = nextNode;
            node._prev = prevNode;
            prevNode._next = node;
            nextNode._prev = node;
            
            this._head = node;
        }
        return this;
    }
    
    @Override
    public String toString() {
        String result = "Doubly Linked List: ";
        
        Node temp = this._head;
        
        do {
            result += temp;
            if (temp != null) {
                temp = temp._next;
            }
        } while ((temp != null) && (temp != this._head));
        
        return result;
    } // toString()
    
    @Override
    public int countOccurence(T data) throws IndexOutOfBoundsException {
        return 0;
    }
    
    @Override
    public T getNth(int index) throws IndexOutOfBoundsException {
        return null;
    }
    
    @Override
    public LinkedList<T>[] AlternatingSplit() {
        return null;
    }
    
    @Override
    public LinkedList<T> AlternatingMerge(LinkedList<T> list2) {
        return null;
    }
    
    @Override
    public void insertAt(int index, T data) throws IndexOutOfBoundsException {
    }
    
    @Override
    public void push(T data) {
    }
    
    @Override
    public LinkedList<T> insertSorted(T data) {
        return null;
    }
    
    @Override
    public void moveNode(LinkedList<T> inputList) throws NullPointerException {
    }
    
    @Override
    public void purge() {
    }
    
    @Override
    public void remove(T data) {
    }
    
    @Override
    public void removeNode(int index) throws IndexOutOfBoundsException {
    }
    
    @Override
    public void reverse() {
    }
    
    @Override
    public void setAt(int index, T data) throws IndexOutOfBoundsException {
    }
    
    @Override
    public int size() {
        return 0;
    }
    
    @Override
    public LinkedList<T>[] split(int numberOfSplits) {
        return null;
    }
    
    Node _head = null;
    
    private class Node {
        public Node(T data) {
            this(data, null, null);
        }
        
        private Node(T data, Node prev, Node next) {
            this._data = data;
            _prev = prev;
            _next = next;
        }
        
        @Override
        public String toString() {
            String result = "";
            if (this._prev == null) {
                result += "// << ";
            } else {
                result += " << ";
            }
            result += this._data;
            if (this._next == null) {
                result += " >>>> //";
            } else {
                result += " >>>> ";
            }
            return result;
        } // toString()
        
        T    _data;
        Node _prev;
        Node _next;
    }
}
