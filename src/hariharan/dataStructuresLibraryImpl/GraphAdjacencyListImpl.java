/**
 * 
 */
package hariharan.dataStructuresLibraryImpl;

import hariharan.dataStructuresLibrary.Graph;

/**
 * Provides an implementation of the graph interface using adjacency list as
 * implementation
 * 
 * @author hariharan
 */
public class GraphAdjacencyListImpl<T extends Comparable<? super T>> implements Graph<T> {
    /**
     * 
     */
    public GraphAdjacencyListImpl() {
        _listOfVertices = null;
    } // GraphAdjacencyListImpl()
    
    private LinkedListImpl<Vertex> _listOfVertices;
    
    @Override
    public void addVertex(Vertex vertex) {
        if (_listOfVertices == null) {
            _listOfVertices = new LinkedListImpl<Vertex>(vertex);
        } else {
            _listOfVertices = (LinkedListImpl<Vertex>) _listOfVertices.append(vertex);
        }
    } // addVertex()
    
    @Override
    public void addEdge(Vertex fromVertex, Edge edge) {
        fromVertex.addEdge(edge);
    } // addEdge()
    
    @Override
    public void addEdge(Vertex fromVertex, Vertex toVertex) {
        addEdge(fromVertex, new Edge(fromVertex, toVertex));
    } // addEdge()
    
    @Override
    public String toString() {
        return "<graph> <vertices> " + _listOfVertices + " </vertices> </graph>";
    }
    
    public class Vertex implements Comparable<Vertex> {
        public Vertex() {
            this(null, null);
        }
        
        public Vertex(T value) {
            this(value, null);
        }
        
        private Vertex(T value, LinkedListImpl<Edge> listOfEdges) {
            _value = value;
            _listOfEdges = listOfEdges;
        }
        
        public void addEdge(Edge edge) {
            if (_listOfEdges == null) {
                _listOfEdges = new LinkedListImpl<Edge>(edge);
            } else {
                _listOfEdges.push(edge);
            }
        } // addEdge()
        
        @Override
        public String toString() {
            String result = "<Vertex>";
            result += " <_value> " + _value + " </_value> ";
            if (_listOfEdges != null) {
                Edge edge = null;
                for (int i = _listOfEdges.size() - 1; i >= 0; i--) {
                    edge = _listOfEdges.getNth(i);
                    result += "<edge> ";
                    result += "<_weight> " + edge.getWeight() + "/<_weight>";
                    result += "<_toVertex> " + edge.getToVertex().getValue() + "/<_toVertex>";
                    result += "/<edge> ";
                }
            }
            return result + "/<Vertex>"; // "<Vertex>\n\t<_value> " + _value +
                                         // " </_value>\n\t<_listOfEdges> " +
                                         // _listOfEdges +
                                         // " </_listOfEdges>\n/<Vertex>";
        }
        
        @Override
        public int compareTo(GraphAdjacencyListImpl<T>.Vertex vertex) {
            int result = -1;
            
            if (this == vertex) {
                return 0;
            }
            
            if (vertex == null) {
                return result;
            }
            
            T value = vertex.getValue();
            if (_value == null) {
                if (value == null) {
                    result = 0;
                }
            } else {
                if (value == null) {
                    result = -1;
                } else {
                    result = _value.compareTo(value);
                }
            }
            
            if (result != 0) {
                return result;
            }
            
            LinkedListImpl<Edge> listOfEdges = vertex.getListOfEdges();
            if (_listOfEdges == null) {
                if (listOfEdges == null) {
                    result = 0;
                }
            } else {
                if (listOfEdges == null) {
                    result = -1;
                } else {
                    if (false == _listOfEdges.equals(listOfEdges)) {
                        result = -1;
                    } else {
                        result = 0;
                    }
                }
            }
            
            return result;
        }// compareTo()
        
        public LinkedListImpl<Edge> getListOfEdges() {
            return _listOfEdges;
        }
        
        public T getValue() {
            return _value;
        }
        
        private T                    _value;
        private LinkedListImpl<Edge> _listOfEdges;
        
    } // class Vertex
    
    public class Edge implements Comparable<Edge> {
        public Edge(Vertex fromVertex, Vertex toVertex) {
            this(fromVertex, toVertex, false, 0);
        } // Edge(Vertex fromVertex, Vertex toVertex)
        
        public Edge(Vertex fromVertex, Vertex toVertex, boolean isDirected, int weight) {
            _fromVertex = fromVertex;
            _toVertex = toVertex;
            _isDirected = isDirected;
            _weight = weight;
        } // Edge(Vertex fromVertex, Vertex toVertex, boolean isDirected, int
          // weight)
        
        @Override
        public int compareTo(Edge edge) {
            int result = -1;
            
            if (this == edge) {
                return 0;
            }
            
            if ((_fromVertex == null) || (_toVertex == null)) {
                return result;
            }
            
            result = (_fromVertex.compareTo(edge._fromVertex));
            
            if (result != 0) {
                return result;
            }
            
            result = (_toVertex.compareTo(edge._toVertex));
            
            if (result != 0) {
                return result;
            }
            
            if (_weight != edge._weight) {
                result = -1;
            }
            
            if (result != 0) {
                return result;
            }
            
            if (_isDirected != edge._isDirected) {
                result = -1;
            }
            
            return result;
        } // compareTo()
        
        @Override
        public String toString() {
            String result = "<Edge> {from [ " + _fromVertex + " ]; to [ " + _toVertex + " ]; weight [ " + _weight + " ]; isDirected [ "
                    + _isDirected + " ]; } </Edge>";
            return result;
        }
        
        /**
         * @return the _fromVertex
         */
        public Vertex getFromVertex() {
            return this._fromVertex;
        }
        
        /**
         * @param _fromVertex
         *            the _fromVertex to set
         */
        public void setFromVertex(Vertex _fromVertex) {
            this._fromVertex = _fromVertex;
        }
        
        /**
         * @return the _toVertex
         */
        public Vertex getToVertex() {
            return this._toVertex;
        }
        
        /**
         * @param _toVertex
         *            the _toVertex to set
         */
        public void setToVertex(Vertex _toVertex) {
            this._toVertex = _toVertex;
        }
        
        /**
         * @return the _isDirected
         */
        public boolean isDirected() {
            return this._isDirected;
        }
        
        /**
         * @param _isDirected
         *            the _isDirected to set
         */
        public void setIsDirected(boolean _isDirected) {
            this._isDirected = _isDirected;
        }
        
        /**
         * @return the _weight
         */
        public int getWeight() {
            return this._weight;
        }
        
        /**
         * @param _weight
         *            the _weight to set
         */
        public void seWeight(int _weight) {
            this._weight = _weight;
        }
        
        private Vertex  _fromVertex;
        private Vertex  _toVertex;
        private boolean _isDirected;
        private int     _weight;
    } // class Edge
    
} // class GraphAdjacencyListImpl
