package hariharan.dataStructuresLibrary;

import hariharan.dataStructuresLibraryImpl.GraphAdjacencyListImpl;

public interface Graph<T extends Comparable<? super T>> {
    /**
     * Adds a new vertex to the graph.
     * 
     * @param vertex
     *            The vertex to be added
     */
    public void addVertex(GraphAdjacencyListImpl<T>.Vertex vertex);
    
    /**
     * Creates a new Edge in the graph
     * 
     * @param fromVertex
     *            The starting vertex for the edge
     * @param toVertex
     *            The ending vertex of the edge
     */
    public void addEdge(GraphAdjacencyListImpl<T>.Vertex fromVertex, GraphAdjacencyListImpl<T>.Vertex toVertex);
    
    /**
     * Creates a new Edge in the graph
     * 
     * @param fromVertex
     *            The starting vertex for the edge
     * @param toVertex
     *            The ending vertex of the edge
     */
    public void addEdge(GraphAdjacencyListImpl<T>.Vertex fromVertex, GraphAdjacencyListImpl<T>.Edge edge);
}
