package hariharan.dataStructuresLibrary;

public interface LinkedList<T> {
    
    /**
     * Count the number of times a node occurs in the list.
     * 
     * @param data
     *            The nodes containing this data will be searched
     * @return The number of times <data> is found in the linked list
     * @throws IndexOutOfBoundsException
     */
    public int countOccurence(T data) throws IndexOutOfBoundsException;
    
    /**
     * Get the value of the nth node in the linked list
     * 
     * @param index
     *            the index of the node whose data is to be found
     * @return the value of the nth node, if found
     * @throws IndexOutOfBoundsException
     *             if the index is out of bounds of the list
     */
    public T getNth(int index) throws IndexOutOfBoundsException;
    
    /**
     * Splits the input list into 2 smaller lists such that each list contains
     * alternate elements of the original list. The smaller lists are returned
     * as an array.
     */
    public LinkedList<T>[] AlternatingSplit();
    
    /**
     * Merges list with input list list2 into a larger lists such that the
     * larger contains alternate elements of the original lists. The head of the
     * larger list is returned.
     */
    public LinkedList<T> AlternatingMerge(LinkedList<T> list2);
    
    /**
     * insert new node to the list at position index. The calue of the node is
     * provided by <data>
     * 
     * @param index
     *            Zero-based index in the list where the new node will be
     *            inserted
     * @param data
     *            value of the new node
     * @throws IndexOutOfBoundsException
     */
    public void insertAt(int index, T data) throws IndexOutOfBoundsException;
    
    /**
     * Push a new node at beginning of the list
     * 
     * @param data
     *            The value of the new node which will be pushed
     */
    public void push(T data);
    
    /**
     * Append a new node at the end of the linked list.
     * 
     * @param data
     *            The value of the new node which will be appended to the end of
     *            the list.
     */
    public LinkedList<T> append(T data);
    
    /**
     * Create new node. Add input data as pay-load to this node. Insert node in
     * ascending order to the list.
     */
    public LinkedList<T> insertSorted(T data);
    
    /**
     * Returns the new head node.
     */
    /**
     * Moves the head node of <inputList> as the head node of <this> list.
     * 
     * @param inputList
     *            the list whose head node will be made the head of <this> list.
     *            Please note that the head node of <inputList> will change as a
     *            result of calling this method.
     * @throws NullPointerException
     *             If <iinpoutList> is null.
     */
    public void moveNode(LinkedList<T> inputList) throws NullPointerException;
    
    /** delete all nodes in the list */
    public void purge();
    
    /**
     */
    /**
     * Remove all nodes containing the value provided by <data>, if found in the
     * list.
     * 
     * @param data
     *            The value of the node that should be deleted.
     */
    public void remove(T data);
    
    /**
     * Remove node at (zero based)index provided as input. Returns the new head
     * of the list.
     */
    public void removeNode(int index) throws IndexOutOfBoundsException;
    
    /**
     * Reverse the linked list
     */
    public void reverse();
    
    /** Set input data as pay-load for the node at position pointed by index */
    
    /** Return the size of linked list */
    public int size();
    
    /**
     * Splits the linked list into several smaller lists as given by
     * <numberOfSplits>. Returns an array of split lists.
     * 
     * @param numberOfSplits
     *            The number of sub-lists that this list should be split into
     * @return
     */
    public LinkedList<T>[] split(int numberOfSplits);
    
    void setAt(int index, T data) throws IndexOutOfBoundsException;
}
