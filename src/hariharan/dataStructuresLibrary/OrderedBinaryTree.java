/**
 * 
 */
package hariharan.dataStructuresLibrary;

import hariharan.dataStructuresLibraryImpl.CircularDoubleLinkedList;

/**
 * @author hariharan
 * 
 */
public interface OrderedBinaryTree<T extends Comparable<? super T>> {
    
    /**
     * Inserts a new node into the tree in an ordered fashion (in-order fashion)
     * 
     * @param data
     *            : The pay-load to add
     * @return Root node of the new tree
     */
    public OrderedBinaryTree<T> insert(T data);
    
    /**
     * Searches for a node with the value <data> in the tree and returns true if
     * found.
     * 
     * @param data
     *            The value that must be searched in the tree
     * @return true if <data> was found in the tree; false otherwise.
     */
    public boolean isPresent(T data);
    
    /**
     * Returns the root value of the tree
     * 
     * @return The value of the root node of the tree
     */
    public T getRoot();
    
    /**
     * Returns the number of nodes in the linked list
     * 
     * @return number of nodes in the linked list
     */
    public int size();
    
    /**
     * Returns the maximum depth of the tree from root to deepest leaf
     * 
     * @return The maximum depth of the tree from root to deepest leaf
     */
    public int maxDepth();
    
    /**
     * Finds the node with the minimum value in the binary tree and returns the
     * value.
     * 
     * @return The minimum value in the binary tree.
     */
    public T minValue();
    
    /**
     * Prints the node of the binary tree in inorder fashion.
     * 
     * @return The sting listing out values in the tree.
     */
    public String printInorder();
    
    /**
     * Prints the node of the binary tree in postorder fashion.
     * 
     * @return The sting listing out values in the tree.
     */
    public String printPostorder();
    
    /**
     * prints out all of its root-to-leaf paths, one per line.
     * 
     * @return True if there is a path from the root down to a leaf, such that
     *         adding up all the values along the path equals the given sum.
     */
    public void printPaths();
    
    /**
     * Creates a mirrored tree of this tree.
     * 
     * @return Root node of the mirrored tree.
     */
    public void mirror();
    
    /**
     * Given two binary trees, return true if they are structurally identical --
     * they are made of nodes with the same values arranged in the same way.
     * 
     * @param tree2
     *            The tree to check for sameness
     * @return true if both trees are identical, false otherwise.
     */
    public boolean isSameTree(OrderedBinaryTree<T> tree2);
    
    /**
     * This method returns the count of all possible binary search trees given
     * the number of nodes as input.
     * 
     * @param nodesCount
     *            Number of nodes in the tree
     * @return The total count of the possible structurally different binary
     *         search trees
     */
    public int countBinarySearchTrees(int nodesCount);
    
    /**
     * Converts the tree into a double linked list
     * 
     * @return Link to the double linked list
     */
    public CircularDoubleLinkedList<T> treeToDoublyLinkedList();
}