/**
 * 
 */
package hariharan.dataStructuresLibraryImplTest;

import static org.junit.Assert.fail;
import hariharan.dataStructuresLibraryImpl.GraphAdjacencyListImpl;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author hariharan
 * 
 */
public class VertexTest {
    
    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }
    
    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }
    
    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }
    
    /**
     * Test method for
     * {@link hariharan.dataStructuresLibraryImpl.GraphAdjacencyListImpl.Vertex#Vertex()}
     * .
     */
    @Test
    public final void testVertex() {
        GraphAdjacencyListImpl<Integer> graph = new GraphAdjacencyListImpl<Integer>();
        GraphAdjacencyListImpl<Integer>.Vertex vertex = graph.new Vertex();
        if (vertex.getValue() != null)
            fail();
        if (vertex.getListOfEdges() != null)
            fail();
        System.out.println(vertex);
    }
    
    /**
     * Test method for
     * {@link hariharan.dataStructuresLibraryImpl.GraphAdjacencyListImpl.Vertex#Vertex(java.lang.Comparable)}
     * .
     */
    @Test
    public final void testVertexT() {
        GraphAdjacencyListImpl<Integer> graph = new GraphAdjacencyListImpl<Integer>();
        GraphAdjacencyListImpl<Integer>.Vertex vertex = graph.new Vertex(53);
        if (vertex.getValue() != 53)
            fail();
        if (vertex.getListOfEdges() != null)
            fail();
        System.out.println(vertex);
    }
    
    /**
     * Test method for
     * {@link hariharan.dataStructuresLibraryImpl.GraphAdjacencyListImpl.Vertex#addEdge(hariharan.dataStructuresLibraryImpl.GraphAdjacencyListImpl.Edge)}
     * .
     */
    @Test
    public final void testAddEdge() {
        GraphAdjacencyListImpl<Integer> graph = new GraphAdjacencyListImpl<Integer>();
        GraphAdjacencyListImpl<Integer>.Vertex vertex1 = graph.new Vertex(21);
        GraphAdjacencyListImpl<Integer>.Vertex vertex2 = graph.new Vertex(45);
        GraphAdjacencyListImpl<Integer>.Vertex vertex3 = graph.new Vertex(5);
        GraphAdjacencyListImpl<Integer>.Edge edge2 = graph.new Edge(vertex1, vertex2, true, 2);
        GraphAdjacencyListImpl<Integer>.Edge edge3 = graph.new Edge(vertex1, vertex3, true, 2);
        GraphAdjacencyListImpl<Integer>.Edge edge4 = graph.new Edge(vertex2, vertex3, false, 4);
        
        graph.addVertex(vertex1);
        graph.addVertex(vertex2);
        
        vertex1.addEdge(edge2);
        if (vertex1.getListOfEdges().countOccurence(edge2) != 1)
            fail();
        if (vertex1.getListOfEdges().size() != 1)
            fail();
        
        vertex1.addEdge(edge3);
        if (vertex1.getListOfEdges().countOccurence(edge3) != 1)
            fail();
        if (vertex1.getListOfEdges().size() != 2)
            fail();
        
        vertex2.addEdge(edge4);
        if (vertex2.getListOfEdges().countOccurence(edge4) != 1)
            fail();
        
        System.out.println(graph);
    }
    
    /**
     * Test method for
     * {@link hariharan.dataStructuresLibraryImpl.GraphAdjacencyListImpl.Vertex#compareTo(hariharan.dataStructuresLibraryImpl.GraphAdjacencyListImpl.Vertex)}
     * .
     */
    @Test
    public final void testCompareTo() {
        GraphAdjacencyListImpl<Integer> graph = new GraphAdjacencyListImpl<Integer>();
        GraphAdjacencyListImpl<Integer>.Vertex v1 = graph.new Vertex();
        GraphAdjacencyListImpl<Integer>.Vertex v2 = graph.new Vertex(53);
        
        if (v1.compareTo(v1) != 0)
            fail();
        if (v2.compareTo(v2) != 0)
            fail();
        if (v1.compareTo(v2) == 0)
            fail();
        if (v2.compareTo(v1) == 0)
            fail();
    }
    
}
