/**
 * 
 */
package hariharan.dataStructuresLibraryImplTest;

import static org.junit.Assert.fail;
import hariharan.dataStructuresLibraryImpl.GraphAdjacencyListImpl;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author hariharan
 * 
 */
public class EdgeTest {
    
    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }
    
    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }
    
    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }
    
    /**
     * Test method for
     * {@link hariharan.dataStructuresLibraryImpl.GraphAdjacencyListImpl.Edge#Edge(hariharan.dataStructuresLibraryImpl.GraphAdjacencyListImpl.Vertex, hariharan.dataStructuresLibraryImpl.GraphAdjacencyListImpl.Vertex)}
     * .
     */
    @Test
    public final void testEdgeVertexVertex() {
        GraphAdjacencyListImpl<Integer> graph = new GraphAdjacencyListImpl<Integer>();
        
        GraphAdjacencyListImpl<Integer>.Edge edge = graph.new Edge(null, null);
        System.out.println(edge);
        if (edge.getFromVertex() != null || edge.getToVertex() != null)
            fail();
        edge = null;
        
        GraphAdjacencyListImpl<Integer>.Vertex vertex1 = graph.new Vertex(1);
        GraphAdjacencyListImpl<Integer>.Vertex vertex2 = graph.new Vertex(2);
        edge = graph.new Edge(vertex1, vertex2);
        System.out.println(edge);
        if (edge.getFromVertex() != vertex1 || edge.getToVertex() != vertex2 || edge.getWeight() != 0 || edge.isDirected() != false)
            fail();
        edge = null;
        vertex1 = null;
        vertex2 = null;
    }
    
    /**
     * Test method for
     * {@link hariharan.dataStructuresLibraryImpl.GraphAdjacencyListImpl.Edge#Edge(hariharan.dataStructuresLibraryImpl.GraphAdjacencyListImpl.Vertex, hariharan.dataStructuresLibraryImpl.GraphAdjacencyListImpl.Vertex, boolean, int)}
     * .
     */
    @Test
    public final void testEdgeVertexVertexBooleanInt() {
        GraphAdjacencyListImpl<Integer> graph = new GraphAdjacencyListImpl<Integer>();
        
        GraphAdjacencyListImpl<Integer>.Edge edge = graph.new Edge(null, null);
        System.out.println(edge);
        if (edge.getFromVertex() != null || edge.getToVertex() != null)
            fail();
        edge = null;
        
        GraphAdjacencyListImpl<Integer>.Vertex vertex1 = graph.new Vertex(1);
        GraphAdjacencyListImpl<Integer>.Vertex vertex2 = graph.new Vertex(2);
        edge = graph.new Edge(vertex1, vertex2, true, 4);
        System.out.println(edge);
        if (edge.getFromVertex() != vertex1 || edge.getToVertex() != vertex2 || edge.getWeight() != 4 || edge.isDirected() != true)
            fail();
        edge = null;
        vertex1 = null;
        vertex2 = null;
    }
    
    /**
     * Test method for
     * {@link hariharan.dataStructuresLibraryImpl.GraphAdjacencyListImpl.Edge#compareTo(hariharan.dataStructuresLibraryImpl.GraphAdjacencyListImpl.Edge)}
     * .
     */
    @Test
    public final void testCompareTo() {
        GraphAdjacencyListImpl<Integer> graph = new GraphAdjacencyListImpl<Integer>();
        
        GraphAdjacencyListImpl<Integer>.Edge edge1 = graph.new Edge(null, null);
        GraphAdjacencyListImpl<Integer>.Vertex vertex1 = graph.new Vertex(1);
        GraphAdjacencyListImpl<Integer>.Vertex vertex2 = graph.new Vertex(2);
        GraphAdjacencyListImpl<Integer>.Edge edge2 = graph.new Edge(vertex1, vertex2, true, 2);
        
        int result = edge1.compareTo(edge2);
        
        System.out.println("<Test 1>");
        System.out.println("\t edge1 = " + edge1);
        System.out.println("\t edge2 = " + edge2);
        System.out.println("\t result of edge1.compareTo(edge2) = " + result);
        System.out.println("/<Test 1>");
        
        if (result == 0)
            fail();
        
        result = edge2.compareTo(edge1);
        
        System.out.println("<Test 2>");
        System.out.println("\t edge1 = " + edge1);
        System.out.println("\t edge2 = " + edge2);
        System.out.println("\t result of edge2.compareTo(edge1) = " + result);
        System.out.println("/<Test 2>");
        
        if (result >= 0)
            fail();
        
        GraphAdjacencyListImpl<Integer>.Vertex vertex3 = graph.new Vertex(3);
        GraphAdjacencyListImpl<Integer>.Edge edge3 = graph.new Edge(vertex2, vertex3, false, 4);
        result = edge3.compareTo(edge2);
        
        System.out.println("<Test 3>");
        System.out.println("\t edge3 = " + edge3);
        System.out.println("\t edge2 = " + edge2);
        System.out.println("\t result of edge3.compareTo(edge2) = " + result);
        System.out.println("/<Test 3>");
        
        if (result >= 0)
            fail();
        
        result = edge3.compareTo(edge3);
        
        System.out.println("<Test 4>");
        System.out.println("\t edge3 = " + edge3);
        System.out.println("\t result of edge3.compareTo(edge3) = " + result);
        System.out.println("/<Test 4>");
        
        if (result != 0)
            fail();
        
        edge1 = null;
        edge2 = null;
        edge3 = null;
        vertex1 = null;
        vertex2 = null;
    }
}
