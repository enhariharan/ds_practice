/**
 * 
 */
package hariharan.dataStructuresLibraryImplTest;

import hariharan.dataStructuresLibrary.LinkedList;
import hariharan.dataStructuresLibraryImpl.CircularDoubleLinkedList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author hariharan
 * 
 */
public class DoubleLinkedListTest {
    
    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }
    
    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }
    
    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }
    
    /**
     * Test method for
     * {@link hariharan.dataStructuresLibraryImpl.CircularDoubleLinkedList#append(int)}
     * .
     */
    @Test
    public final void testAppend() {
        LinkedList<Integer> list = new CircularDoubleLinkedList<Integer>();
        list = list.append(0).append(6).append(4);
        System.out.println(list);
    }
    
}
