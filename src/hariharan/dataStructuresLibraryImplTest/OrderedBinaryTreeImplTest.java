package hariharan.dataStructuresLibraryImplTest;

import static org.junit.Assert.fail;
import hariharan.dataStructuresLibrary.OrderedBinaryTree;
import hariharan.dataStructuresLibraryImpl.CircularDoubleLinkedList;
import hariharan.dataStructuresLibraryImpl.OrderedBinaryTreeImpl;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class OrderedBinaryTreeImplTest {
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }
    
    @Before
    public void setUp() throws Exception {
    }
    
    @After
    public void tearDown() throws Exception {
    }
    
    @Test
    public final void testCreateTree() {
        String method = "testCreateTree()";
        System.err.println("<" + method + ">");
        
        OrderedBinaryTree<Integer> tree = new OrderedBinaryTreeImpl<Integer>();
        System.err.println("\t - At init - " + tree);
        
        tree = new OrderedBinaryTreeImpl<Integer>(5).insert(0).insert(7).insert(2).insert(1);
        System.err.println("\t - After new OrderedBinaryTreeImpl<Integer>(5) - " + tree);
        
        System.err.println("<\\" + method + ">");
    }
    
    @Test
    public final void testGetRoot() {
        String method = "testGetRoot()";
        System.err.println("<" + method + ">");
        
        OrderedBinaryTree<Integer> tree = new OrderedBinaryTreeImpl<Integer>();
        System.err.println("\t - At init: " + tree);
        Integer root = null;
        root = tree.getRoot();
        System.err.println("\t - Root: " + root);
        if (root != -999)
            fail();
        
        tree = new OrderedBinaryTreeImpl<Integer>(5);
        System.err.println("\t - After new OrderedBinaryTreeImpl<Integer>(5)" + tree);
        root = tree.getRoot();
        System.err.println("\t - Root: " + root);
        if (root != 5)
            fail();
        
        System.err.println("<\\" + method + ">");
    }
    
    @Test
    public final void testInsert() {
        String method = "testInsert()";
        System.err.println("<" + method + ">");
        
        OrderedBinaryTree<Integer> tree = new OrderedBinaryTreeImpl<Integer>();
        System.err.println("\t - At init: " + tree);
        
        tree.insert(14);
        if (tree.isPresent(14) == false || tree.getRoot() != 14) {
            fail();
        }
        System.err.println("\t - After tree.insert(14): " + tree);
        
        tree.insert(10);
        if (tree.isPresent(10) == false || tree.getRoot() != 14) {
            fail();
        }
        System.err.println("\t - tree.insert(10): " + tree);
        System.err.println("<\\" + method + ">");
    }
    
    @Test
    public final void testIsPresent() {
        String method = "testIsPresent()";
        System.err.println("<" + method + ">");
        
        OrderedBinaryTree<Integer> tree = new OrderedBinaryTreeImpl<Integer>();
        System.err.println("\t - At init - " + tree);
        
        boolean flag = tree.isPresent(14);
        if (flag != false) {
            fail();
        }
        System.err.println("\t - tree.isPresent(14) returned: " + flag);
        
        tree.insert(14);
        flag = tree.isPresent(14);
        if (flag == false) {
            fail();
        }
        System.err.println("\t - After tree.insert(14): " + tree);
        System.err.println("\t - tree.isPresent(14) returned: " + flag);
        
        tree.insert(10);
        flag = tree.isPresent(10);
        if (flag == false) {
            fail();
        }
        System.err.println("\t - After tree.insert(10): " + tree);
        System.err.println("\t - tree.isPresent(10) returned: " + flag);
        
        tree.insert(21);
        flag = tree.isPresent(21);
        if (flag == false) {
            fail();
        }
        System.err.println("\t - After tree.insert(21): " + tree);
        System.err.println("\t - tree.isPresent(21) returned: " + flag);
        
        flag = tree.isPresent(103);
        if (flag == true) {
            fail();
        }
        System.err.println("\t - tree.isPresent(103) returned: " + flag);
        System.err.println("<\\" + method + ">");
    }
    
    @Test
    public final void testSize() {
        String method = "testSize()";
        System.err.println("<" + method + ">");
        
        OrderedBinaryTree<Integer> tree = new OrderedBinaryTreeImpl<Integer>();
        System.err.println("\t - At init - " + tree);
        
        int size = tree.size();
        if (size != 0) {
            fail();
        }
        System.err.println("\t - tree.size() returned: " + size);
        
        tree = tree.insert(5);
        System.err.println("\t - tree - " + tree);
        size = tree.size();
        if (size != 1) {
            fail();
        }
        System.err.println("\t - tree.size() returned: " + size);
        
        tree = tree.insert(2);
        System.err.println("\t - tree - " + tree);
        size = tree.size();
        if (size != 2) {
            fail();
        }
        System.err.println("\t - tree.size() returned: " + size);
        
        tree = tree.insert(11);
        System.err.println("\t - tree - " + tree);
        size = tree.size();
        if (size != 3) {
            fail();
        }
        System.err.println("\t - tree.size() returned: " + size);
        
        tree = tree.insert(3).insert(1).insert(7).insert(13);
        System.err.println("\t - tree - " + tree);
        size = tree.size();
        if (size != 7) {
            fail();
        }
        System.err.println("\t - tree.size() returned: " + size);
        
        System.err.println("<\\" + method + ">");
    }
    
    @Test
    public final void testMaxDepth() {
        String method = "testMaxDepth()";
        System.err.println("<" + method + ">");
        
        OrderedBinaryTree<Integer> tree = new OrderedBinaryTreeImpl<Integer>();
        System.err.println("\t - At init - " + tree);
        
        int maxDepth = tree.maxDepth();
        if (maxDepth != 0) {
            fail();
        }
        System.err.println("\t - tree.maxDepth() returned: " + maxDepth);
        
        tree = tree.insert(5);
        System.err.println("\t - tree - " + tree);
        maxDepth = tree.maxDepth();
        if (maxDepth != 0) {
            fail();
        }
        System.err.println("\t - tree.maxDepth() returned: " + maxDepth);
        
        tree = tree.insert(2);
        System.err.println("\t - tree - " + tree);
        maxDepth = tree.maxDepth();
        if (maxDepth != 1) {
            fail();
        }
        System.err.println("\t - tree.maxDepth() returned: " + maxDepth);
        
        tree = tree.insert(11);
        System.err.println("\t - tree - " + tree);
        maxDepth = tree.maxDepth();
        if (maxDepth != 1) {
            fail();
        }
        System.err.println("\t - tree.maxDepth() returned: " + maxDepth);
        
        tree = tree.insert(3).insert(1).insert(7).insert(13).insert(17);
        System.err.println("\t - tree - " + tree);
        maxDepth = tree.maxDepth();
        if (maxDepth != 3) {
            fail();
        }
        System.err.println("\t - tree.maxDepth() returned: " + maxDepth);
        
        System.err.println("<\\" + method + ">");
    }
    
    @Test
    public final void testMinValue() {
        String method = "testMinValue()";
        System.err.println("<" + method + ">");
        
        OrderedBinaryTree<Integer> tree = new OrderedBinaryTreeImpl<Integer>();
        System.err.println("\t - At init - " + tree);
        
        int minValue = tree.minValue();
        if (minValue != 0) {
            fail();
        }
        System.err.println("\t - tree.maxDepth() returned: " + minValue + "\n");
        
        tree = tree.insert(7);
        System.err.println("\t - tree before caling tree.minValue(): " + tree);
        minValue = tree.minValue();
        if (minValue != 7) {
            fail();
        }
        System.err.println("\t - tree.maxDepth() returned: " + minValue + "\n");
        
        tree = tree.insert(9).insert(6);
        System.err.println("\t - tree before caling tree.minValue(): " + tree);
        minValue = tree.minValue();
        if (minValue != 6) {
            fail();
        }
        System.err.println("\t - tree.maxDepth() returned: " + minValue + "\n");
        
        tree = tree.insert(4).insert(11).insert(3).insert(5).insert(1).insert(-22);
        System.err.println("\t - tree before caling tree.minValue(): " + tree);
        minValue = tree.minValue();
        if (minValue != -22) {
            fail();
        }
        System.err.println("\t - tree.maxDepth() returned: " + minValue + "\n");
        
        System.err.println("<\\" + method + ">");
    }
    
    @Test
    public final void testPrintInorder() {
        String method = "testPrintInorder()";
        System.err.println("<" + method + ">");
        
        OrderedBinaryTree<Integer> tree = new OrderedBinaryTreeImpl<Integer>();
        System.err.println("\t - At init - " + tree);
        String printedTree = tree.printInorder();
        if (printedTree != null) {
            fail();
        }
        System.err.println("\t - tree.Print() returned: " + printedTree + "\n");
        
        tree = tree.insert(7);
        System.err.println("\t - tree before caling tree.minValue(): " + tree);
        printedTree = tree.printInorder();
        if (printedTree == null) {
            fail();
        }
        System.err.println("\t - tree.Print() returned: " + printedTree + "\n");
        
        tree = tree.insert(9).insert(6);
        System.err.println("\t - tree before caling tree.minValue(): " + tree);
        printedTree = tree.printInorder();
        if (printedTree == null) {
            fail();
        }
        System.err.println("\t - tree.Print() returned: " + printedTree + "\n");
        
        tree = tree.insert(4).insert(11).insert(3).insert(5).insert(1).insert(-22).insert(25).insert(0).insert(70).insert(52).insert(31)
                .insert(60);
        System.err.println("\t - tree before caling tree.minValue(): " + tree);
        printedTree = tree.printInorder();
        if (printedTree == null) {
            fail();
        }
        System.err.println("\t - tree.Print() returned: " + printedTree + "\n");
        
        System.err.println("<\\" + method + ">");
    }
    
    @Test
    public final void testPrintPostorder() {
        String method = "testPrintPostorder()";
        System.err.println("<" + method + ">");
        
        OrderedBinaryTree<Integer> tree = new OrderedBinaryTreeImpl<Integer>();
        System.err.println("\t - At init - " + tree);
        String printedTree = tree.printPostorder();
        if (printedTree != null) {
            fail();
        }
        System.err.println("\t - tree.Print() returned: " + printedTree + "\n");
        
        tree = tree.insert(7);
        System.err.println("\t - tree before caling tree.minValue(): " + tree);
        printedTree = tree.printPostorder();
        if (printedTree == null) {
            fail();
        }
        System.err.println("\t - tree.Print() returned: " + printedTree + "\n");
        
        tree = tree.insert(9).insert(6);
        System.err.println("\t - tree before caling tree.minValue(): " + tree);
        printedTree = tree.printPostorder();
        if (printedTree == null) {
            fail();
        }
        System.err.println("\t - tree.Print() returned: " + printedTree + "\n");
        
        tree = tree.insert(4).insert(11).insert(3).insert(5).insert(1).insert(-22).insert(25).insert(0).insert(70).insert(52).insert(31)
                .insert(60);
        System.err.println("\t - tree before caling tree.minValue(): " + tree);
        printedTree = tree.printPostorder();
        if (printedTree == null) {
            fail();
        }
        System.err.println("\t - tree.Print() returned: " + printedTree + "\n");
        
        System.err.println("<\\" + method + ">");
    }
    
    @Test
    public final void testPrintPaths() {
        String method = "testPrintPaths()";
        System.err.println("<" + method + ">");
        
        OrderedBinaryTree<Integer> tree = new OrderedBinaryTreeImpl<Integer>();
        System.err.println("\t - Tree for printPaths() Test 1 - " + tree);
        tree.printPaths();
        
        tree = tree.insert(5);
        System.err.println("\t - Tree for printPaths() Test 2 - " + tree);
        // tree.insert(5).insert(2).insert(1).insert(3).insert(11).insert(10).insert(14).insert(13);
        tree.printPaths();
        
        tree = new OrderedBinaryTreeImpl<Integer>().insert(5).insert(2);
        System.err.println("\t - Tree for printPaths() Test 3 - " + tree);
        // tree.insert(5).insert(2).insert(1).insert(3).insert(11).insert(10).insert(14).insert(13);
        tree.printPaths();
        
        tree = new OrderedBinaryTreeImpl<Integer>().insert(5).insert(2).insert(1).insert(3).insert(11).insert(10).insert(14).insert(13);
        System.err.println("\t - Tree for printPaths() Test 3 - " + tree);
        tree.printPaths();
        System.err.println("<\\" + method + ">");
    }
    
    @Test
    public final void testMirror() {
        String method = "testMirror()";
        System.err.println("<" + method + ">");
        
        OrderedBinaryTree<Integer> tree = new OrderedBinaryTreeImpl<Integer>();
        System.err.println("\t - Tree for printPaths() Test 1 - " + tree);
        tree.mirror();
        System.err.println("\t - Mirrored Tree for printPaths() Test 1 - " + tree + "\n");
        if (tree.getRoot() != -999)
            fail();
        
        tree = tree.insert(5);
        System.err.println("\t - Tree for printPaths() Test 2 - " + tree);
        tree.mirror();
        System.err.println("\t - Mirrored Tree for printPaths() Test 2 - " + tree + "\n");
        if (tree.getRoot() != 5)
            fail();
        
        tree = new OrderedBinaryTreeImpl<Integer>().insert(5).insert(2);
        System.err.println("\t - Tree for printPaths() Test 3 - " + tree);
        tree.mirror();
        System.err.println("\t - Mirrored Tree for printPaths() Test 3 - " + tree + "\n");
        if (tree.getRoot() != 5)
            fail();
        
        tree = new OrderedBinaryTreeImpl<Integer>().insert(5).insert(2).insert(1).insert(3).insert(11).insert(10).insert(14).insert(13);
        System.err.println("\t - Tree for printPaths() Test 3 - " + tree);
        tree.mirror();
        System.err.println("\t - Mirrored Tree for printPaths() Test 4 - " + tree + "\n");
        if (tree.getRoot() != 5)
            fail();
        
        System.err.println("<\\" + method + ">");
    }
    
    @Test
    public final void testIsSameTree() {
        String method = "testIsSameTree()";
        System.err.println("<" + method + ">");
        
        OrderedBinaryTree<Integer> tree = new OrderedBinaryTreeImpl<Integer>();
        OrderedBinaryTree<Integer> tree2 = new OrderedBinaryTreeImpl<Integer>();
        System.err.println("\t - Tree for printPaths() Test 1 - " + tree);
        boolean flag = tree.isSameTree(tree2);
        System.err.println("\t - tree.isSameTree() returned - " + flag + "\n");
        if (flag != false) {
            fail();
        }
        
        tree = tree.insert(5);
        tree2 = tree.insert(5);
        System.err.println("\t - Tree for printPaths() Test 2 - " + tree);
        flag = tree.isSameTree(tree2);
        System.err.println("\t - tree.isSameTree() returned - " + flag + "\n");
        if (flag != true) {
            fail();
        }
        
        tree = new OrderedBinaryTreeImpl<Integer>().insert(5).insert(2);
        tree2 = new OrderedBinaryTreeImpl<Integer>().insert(5).insert(2);
        System.err.println("\t - Tree for printPaths() Test 3 - " + tree);
        flag = tree.isSameTree(tree2);
        System.err.println("\t - tree.isSameTree() returned - " + flag + "\n");
        if (flag != true) {
            fail();
        }
        
        tree = new OrderedBinaryTreeImpl<Integer>().insert(5).insert(2).insert(1).insert(3).insert(11).insert(10).insert(14).insert(13);
        tree2 = new OrderedBinaryTreeImpl<Integer>().insert(5).insert(2).insert(1).insert(3).insert(11).insert(10).insert(14).insert(13)
                .insert(0);
        System.err.println("\t - Tree for printPaths() Test 3 - " + tree);
        flag = tree.isSameTree(tree2);
        System.err.println("\t - tree.isSameTree() returned - " + flag + "\n");
        if (flag == true) {
            fail();
        }
        
        System.err.println("<\\" + method + ">");
    }
    
    @Test
    public final void testCountBinarySearchTrees() {
        String method = "testCountBinarySearchTrees()";
        System.err.println("<" + method + ">");
        
        OrderedBinaryTree<Integer> tree = new OrderedBinaryTreeImpl<Integer>();
        for (int k = 0; k <= 4; k++) {
            int count = tree.countBinarySearchTrees(k);
            System.err.println("\t - tree.countBinarySearchTrees(" + k + ") returned - " + count + "\n");
        }
        
        System.err.println("<\\" + method + ">");
    }
    
    @Test
    public final void testTreeToDoublyLinkedList() {
        String method = "testTreeToDoublyLinkedList()";
        System.err.println("<" + method + ">");
        
        OrderedBinaryTree<Integer> tree = new OrderedBinaryTreeImpl<Integer>();
        
        System.err.println("\t Before calling tree.testTreeToDoublyLinkedList() - " + tree + "\n");
        CircularDoubleLinkedList<Integer> list = tree.treeToDoublyLinkedList();
        System.err.println("\t tree.testTreeToDoublyLinkedList(null) returned this list- " + list + "\n");
        
        tree.insert(9);
        System.err.println("\t Before calling tree.testTreeToDoublyLinkedList() - " + tree + "\n");
        list = tree.treeToDoublyLinkedList();
        System.err.println("\t tree.testTreeToDoublyLinkedList(null) returned this list- " + list + "\n");
        if (list == null)
            fail();
        
        tree.insert(5).insert(15).insert(23).insert(3).insert(51);
        System.err.println("\t Before calling tree.testTreeToDoublyLinkedList() - " + tree + "\n");
        list = tree.treeToDoublyLinkedList();
        System.err.println("\t tree.testTreeToDoublyLinkedList(null) returned this list- " + list + "\n");
        if (list == null)
            fail();
        
        System.err.println("<\\" + method + ">");
    }
}
