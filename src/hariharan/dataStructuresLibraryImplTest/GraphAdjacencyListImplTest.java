package hariharan.dataStructuresLibraryImplTest;

import hariharan.dataStructuresLibraryImpl.GraphAdjacencyListImpl;
import hariharan.dataStructuresLibraryImpl.GraphAdjacencyListImpl.Vertex;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class GraphAdjacencyListImplTest {
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }
    
    @Before
    public void setUp() throws Exception {
    }
    
    @After
    public void tearDown() throws Exception {
    }
    
    @Test
    public final void testAddVertex() {
        System.out.println("<GraphAdjacencyListImplTest.testAddVertex()> \n");
        GraphAdjacencyListImpl<String> graph = new GraphAdjacencyListImpl<String>();
        graph.addVertex(graph.new Vertex("Vertex One"));
        graph.addVertex(graph.new Vertex("Vertex Two"));
        graph.addVertex(graph.new Vertex("Vertex Three"));
        graph.addVertex(graph.new Vertex("Vertex Four"));
        System.out.println("\t" + graph);
        System.out.println("\n /<GraphAdjacencyListImplTest.testAddVertex()>");
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Test
    public final void testAddEdge() {
        System.out.println("<GraphAdjacencyListImplTest.testAddEdge()> \n");
        
        GraphAdjacencyListImpl<String> graph = new GraphAdjacencyListImpl<String>();
        System.out.println("<GraphAdjacencyListImplTest.testAddEdge()> - After new GraphAdjacencyListImpl<String>(): \n" + graph);
        
        Vertex v1 = graph.new Vertex("v1");
        graph.addVertex(v1);
        System.out.println("<GraphAdjacencyListImplTest.testAddEdge()> - After graph.addVertex(v1): \n" + graph);
        
        Vertex v2 = graph.new Vertex("v2");
        graph.addVertex(v2);
        System.out.println("<GraphAdjacencyListImplTest.testAddEdge()> - After graph.addVertex(v2): \n" + graph);
        
        graph.addEdge(v1, v2);
        System.out.println("<GraphAdjacencyListImplTest.testAddEdge()> - After graph.addEdge(v1, v2): \n" + graph);
        
        System.out.println("\n /<GraphAdjacencyListImplTest.testAddEdge()>");
    } // testAddEdge()
}
