package hariharan.dataStructuresLibraryImplTest;

import static org.junit.Assert.fail;
import hariharan.dataStructuresLibrary.LinkedList;
import hariharan.dataStructuresLibraryImpl.LinkedListImpl;

import org.junit.Test;

public class LinkedListImplTest {
    
    @Test
    public final void testLinkedListCreation() {
        LinkedList<Integer> list = new LinkedListImpl<Integer>(5);
        if (list.getNth(0) != 5) {
            fail();
        }
    }
    
    @Test
    public final void testSortedMerge() {
        LinkedListImpl<Integer> list1 = new LinkedListImpl<Integer>(2);
        LinkedListImpl<Integer> list2 = null;
        System.err.println("list1: " + list1 + "\nlist2 :" + list2);
        list1.sortedMerge(list2);
        System.err.println("Merged list: " + list1 + "\n");
        if (list1.size() != 1)
            fail();
        
        list1 = new LinkedListImpl<Integer>(0);
        list2 = new LinkedListImpl<Integer>(2);
        System.err.println("list1: " + list1 + "\nlist2 :" + list2);
        list1.sortedMerge(list2);
        System.err.println("Merged list: " + list1 + "\n");
        if (list1.getNth(1) != 2)
            fail();
        
        list1 = new LinkedListImpl<Integer>(2);
        list2 = new LinkedListImpl<Integer>(2);
        System.err.println("list1: " + list1 + "\nlist2 :" + list2);
        list1.sortedMerge(list2);
        System.err.println("Merged list: " + list1 + "\n");
        if (list1.getNth(1) != 2)
            fail();
        
        list1 = new LinkedListImpl<Integer>(2);
        list2 = new LinkedListImpl<Integer>(0);
        System.err.println("list1: " + list1 + "\nlist2 :" + list2);
        list1.sortedMerge(list2);
        System.err.println("Merged list: " + list1 + "\n");
        if (list1.getNth(1) != 2)
            fail();
        
        list1 = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(2).insertSorted(0).insertSorted(4).insertSorted(0).insertSorted(2);
        list2 = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(1).insertSorted(3).insertSorted(5);
        System.err.println("list1: " + list1 + "\nlist2 :" + list2);
        list1.sortedMerge(list2);
        System.err.println("Merged list: " + list1 + "\n");
        if (list1.getNth(list1.size() - 1) != 5)
            fail();
        
        list1 = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(0).insertSorted(2).insertSorted(4).insertSorted(6).insertSorted(8)
                .insertSorted(10);
        list2 = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(1).insertSorted(3).insertSorted(5);
        System.err.println("list1: " + list1 + "\nlist2 :" + list2);
        list1.sortedMerge(list2);
        System.err.println("Merged list: " + list1 + "\n");
        if (list1.getNth(list1.size() - 1) != 10)
            fail();
        
        list1 = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(0).insertSorted(2).insertSorted(4).insertSorted(6).insertSorted(8)
                .insertSorted(10);
        list2 = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(1).insertSorted(3).insertSorted(5).insertSorted(7).insertSorted(9)
                .insertSorted(11).insertSorted(13).insertSorted(15).insertSorted(17).insertSorted(19).insertSorted(21).insertSorted(23)
                .insertSorted(25).insertSorted(27).insertSorted(29);
        System.err.println("list1: " + list1 + "\nlist2 :" + list2);
        list1.sortedMerge(list2);
        System.err.println("Merged list: " + list1 + "\n");
        if (list1.getNth(list1.size() - 1) != 29)
            fail();
        
        list1 = new LinkedListImpl<Integer>(0);
        list2 = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(1).insertSorted(3).insertSorted(5).insertSorted(7).insertSorted(9)
                .insertSorted(11).insertSorted(13).insertSorted(15).insertSorted(17).insertSorted(19).insertSorted(21).insertSorted(23)
                .insertSorted(25).insertSorted(27).insertSorted(29);
        System.err.println("list1: " + list1 + "\nlist2 :" + list2);
        list1.sortedMerge(list2);
        System.err.println("Merged list: " + list1 + "\n");
        if (list1.getNth(list1.size() - 1) != 29)
            fail();
        
        list1 = new LinkedListImpl<Integer>(1000);
        list2 = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(1).insertSorted(3).insertSorted(5).insertSorted(7).insertSorted(9)
                .insertSorted(11).insertSorted(13).insertSorted(15).insertSorted(17).insertSorted(19).insertSorted(21).insertSorted(23)
                .insertSorted(25).insertSorted(27).insertSorted(29);
        System.err.println("list1: " + list1 + "\nlist2 :" + list2);
        list1.sortedMerge(list2);
        System.err.println("Merged list: " + list1 + "\n");
        if (list1.getNth(list1.size() - 1) != 1000)
            fail();
        
        list1 = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(60).insertSorted(62).insertSorted(64).insertSorted(66)
                .insertSorted(68).insertSorted(100);
        list2 = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(1).insertSorted(3).insertSorted(5).insertSorted(7).insertSorted(9)
                .insertSorted(11).insertSorted(13).insertSorted(15).insertSorted(17).insertSorted(19).insertSorted(21).insertSorted(23)
                .insertSorted(25).insertSorted(27).insertSorted(29);
        System.err.println("list1: " + list1 + "\nlist2 :" + list2);
        list1.sortedMerge(list2);
        System.err.println("Merged list: " + list1 + "\n");
        if (list1.getNth(list1.size() - 1) != 100)
            fail();
        
        list1 = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(1).insertSorted(2).insertSorted(3).insertSorted(4).insertSorted(5)
                .insertSorted(6);
        list2 = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(7).insertSorted(9).insertSorted(11).insertSorted(13).insertSorted(15)
                .insertSorted(17).insertSorted(19).insertSorted(21).insertSorted(23).insertSorted(25).insertSorted(27).insertSorted(29);
        System.err.println("list1: " + list1 + "\nlist2 :" + list2);
        list1.sortedMerge(list2);
        System.err.println("Merged list: " + list1 + "\n");
        if (list1.getNth(list1.size() - 1) != 29)
            fail();
    }
    
    @Test
    public final void testAlternatingMerge() {
        LinkedList<Integer> list1 = new LinkedListImpl<Integer>(2);
        LinkedList<Integer> list2 = null;
        System.err.println("list1: " + list1 + "\nlist2 :" + list2);
        LinkedList<Integer> result1 = list1.AlternatingMerge(list2);
        System.err.println("Merged list: " + result1 + "\n");
        
        list1 = new LinkedListImpl<Integer>(0);
        list2 = new LinkedListImpl<Integer>(2);
        System.err.println("list1: " + list1 + "\nlist2 :" + list2);
        result1 = list1.AlternatingMerge(list2);
        System.err.println("Merged list: " + result1 + "\n");
        
        list1 = new LinkedListImpl<Integer>(0).append(2);
        list2 = new LinkedListImpl<Integer>(1).append(3);
        System.err.println("list1: " + list1 + "\nlist2 :" + list2);
        result1 = list1.AlternatingMerge(list2);
        System.err.println("Merged list: " + result1 + "\n");
        
        list1 = new LinkedListImpl<Integer>(0).append(2).append(4).append(6).append(2).append(4).append(6).append(2).append(4).append(6)
                .append(2).append(4).append(6).append(2).append(4).append(6).append(2).append(4).append(6).append(2).append(4).append(6)
                .append(2).append(4).append(6);
        list2 = new LinkedListImpl<Integer>(1).append(3).append(5).append(7).append(9).append(11).append(3).append(5).append(7).append(9)
                .append(11).append(3).append(5).append(7).append(9).append(11).append(3).append(5).append(7).append(9).append(11).append(3)
                .append(5).append(7).append(9).append(11).append(3).append(5).append(7).append(9).append(11).append(3).append(5).append(7)
                .append(9).append(11).append(3).append(5).append(7).append(9).append(11).append(3).append(5).append(7).append(9).append(11)
                .append(3).append(5).append(7).append(9).append(11).append(3).append(5).append(7).append(9).append(11).append(3).append(5)
                .append(7).append(9).append(11).append(3).append(5).append(7).append(9).append(11).append(3).append(5).append(7).append(9)
                .append(11).append(3).append(5).append(7).append(9).append(11).append(3).append(5).append(7).append(9).append(11);
        System.err.println("list1: " + list1 + "\nlist2 :" + list2);
        result1 = list1.AlternatingMerge(list2);
        System.err.println("Merged list: " + result1 + "\n");
    }
    
    @Test
    public final void testAlternatingSplit() {
        LinkedList<Integer> list = new LinkedListImpl<Integer>();
        list = list.append(1).append(2).append(3).append(4).append(5).append(6).append(7).append(8).append(9).append(10).append(11)
                .append(12).append(13).append(14).append(15).append(16).append(17).append(18).append(19).append(20).append(21).append(22)
                .append(23).append(24).append(25).append(26).append(27).append(28).append(29).append(30).append(31).append(32).append(33)
                .append(34).append(35).append(36).append(37).append(38).append(39).append(40).append(41).append(42).append(43).append(44)
                .append(45).append(46).append(47).append(48).append(49).append(50).append(51).append(52).append(53).append(54).append(55)
                .append(56).append(57).append(58).append(59).append(60).append(61).append(62).append(63).append(64).append(65).append(66)
                .append(67).append(68).append(69).append(70).append(71).append(72).append(73).append(74).append(75).append(76).append(77)
                .append(78).append(79).append(80).append(81).append(82).append(83).append(84).append(85).append(86).append(87).append(88)
                .append(89).append(90).append(91).append(92).append(93).append(94).append(95).append(96).append(97).append(98).append(99)
                .append(100);
        System.err.println(list);
        
        LinkedList<Integer>[] splitLists = null;
        splitLists = list.AlternatingSplit();
        System.err.println("split list 1: " + splitLists[0]);
        System.err.println("split list 2: " + splitLists[1]);
    }
    
    @Test
    public final void testCountOccurence() {
        LinkedList<Integer> list = new LinkedListImpl<Integer>(17);
        list.append(1);
        list.append(5);
        list.append(7);
        list.append(13);
        list.append(10);
        list.append(-1);
        list.append(17);
        list.append(1);
        list.append(5);
        list.append(13);
        list.append(17);
        list.append(5);
        list.append(-1);
        list.append(17);
        list.append(17);
        System.err.println(list);
        int count17 = list.countOccurence(17);
        if (count17 != 5)
            fail();
        int count5 = list.countOccurence(5);
        if (count5 != 3)
            fail();
        int countMinus1 = list.countOccurence(-1);
        if (countMinus1 != 2)
            fail();
        int count13 = list.countOccurence(13);
        if (count13 != 2)
            fail();
        int count21 = list.countOccurence(21);
        if (count21 != 0)
            fail();
        System.err.println("Result of countOccurence(17): " + count17 + " countOccurence(5):" + count5 + " countOccurence(-1):"
                + countMinus1 + " countOccurence(13):" + count13 + " countOccurence(21):" + count21);
    }
    
    @Test
    public final void testGetAt() {
        LinkedList<Integer> list = new LinkedListImpl<Integer>(17);
        list.append(1);
        list.append(5);
        list.append(7);
        System.err.println("At init\n" + list);
        
        if (list.getNth(0) != 17)
            fail();
        if (list.getNth(1) != 1)
            fail();
        if (list.getNth(2) != 5)
            fail();
        if (list.getNth(3) != 7)
            fail();
        try {
            System.err.println("The data at index -1 is " + list.getNth(-1));
        } catch (Exception e) {
            System.err.println("getAt(-1) returned an Exception");
        }
        try {
            System.err.println("The data at index 100 is " + list.getNth(-1));
        } catch (Exception e) {
            System.err.println("getAt(100) returned an Exception");
        }
        try {
            System.err.println("The data at index 16 is " + list.getNth(16));
        } catch (Exception e) {
            System.err.println("getAt(16) returned an Exception");
        }
    }
    
    @Test
    public final void testInsertAt() {
        LinkedList<Integer> list = new LinkedListImpl<Integer>(17);
        list.append(1);
        list.append(5);
        list.append(7);
        list.append(13);
        list.append(10);
        list.append(-1);
        list.append(17);
        list.append(1);
        list.append(5);
        list.append(13);
        list.append(17);
        list.append(5);
        list.append(-1);
        list.append(17);
        list.append(17);
        
        list.insertAt(4, 15);
        System.err.println("After insertAt(4, 15)\n" + list);
        if (list.getNth(4) != 15)
            fail();
        
        try {
            list.insertAt(-1, 15);
        } catch (Exception e) {
            System.err.println("insertAt(-1, 15) threw exception");
        }
        try {
            list.insertAt(101, 15);
        } catch (Exception e) {
            System.err.println("After insertAt(101, 15) threw exception");
        }
    }
    
    @Test
    public final void testPush() {
        LinkedList<Integer> list = new LinkedListImpl<Integer>(17);
        list.append(1);
        list.append(5);
        list.append(7);
        System.err.println("At init\n" + list);
        
        list.push(100);
        System.err.println("After insertAtBegin(100)\n" + list);
        if (list.getNth(0) != 100)
            fail();
    }
    
    @Test
    public final void testAppend() {
        LinkedList<Integer> list = new LinkedListImpl<Integer>(17);
        list.append(1);
        if (list.getNth(1) != 1)
            fail();
        System.err.println("After list.append(1)\n" + list);
        
        list.append(5);
        if (list.getNth(2) != 5)
            fail();
        System.err.println("After list.append(5)\n" + list);
        
        list.append(7);
        if (list.getNth(3) != 7)
            fail();
        System.err.println("After list.append(7)\n" + list);
    }
    
    @Test
    public final void testInsertSorted() {
        LinkedList<Integer> sortedList = new LinkedListImpl<Integer>(17).insertSorted(1).insertSorted(5).insertSorted(21).insertSorted(-1);
        System.err.println("Sorted list is :\n" + sortedList);
    }
    
    @Test
    public final void testMergeSort() {
        System.err.println("Input list: " + null);
        LinkedListImpl<Integer> sortedList = new LinkedListImpl<Integer>();
        sortedList.mergeSort();
        System.err.println("Sorted list: " + sortedList + "\n");
        
        LinkedListImpl<Integer> list1 = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(21).append(15).append(42).append(0)
                .append(-1).append(0).append(11).append(10).append(90).append(52).append(90);
        System.err.println(list1);
        list1.mergeSort();
        System.err.println("Sorted list: " + list1);
        if (list1.getNth(0) != -1)
            fail();
        if (list1.getNth(1) != 0)
            fail();
        if (list1.getNth(2) != 0)
            fail();
        if (list1.getNth(3) != 10)
            fail();
        if (list1.getNth(4) != 11)
            fail();
        if (list1.getNth(5) != 15)
            fail();
        if (list1.getNth(6) != 21)
            fail();
        if (list1.getNth(7) != 42)
            fail();
        if (list1.getNth(8) != 52)
            fail();
        if (list1.getNth(9) != 90)
            fail();
        if (list1.getNth(10) != 90)
            fail();
    }
    
    @Test
    public final void testMoveNode() {
        LinkedList<Integer> list1 = new LinkedListImpl<Integer>(17);
        list1.append(1);
        list1.append(5);
        System.err.println("list: " + list1);
        
        LinkedList<Integer> list2 = new LinkedListImpl<Integer>(7);
        list2.append(13);
        list2.append(10);
        System.err.println("list2: " + list2 + "\n");
        
        list1.moveNode(list2);
        System.err.println("list after list.moveNode(list2): " + list1);
        System.err.println("Residual list list2 is: " + list2 + "\n");
        if (list1.getNth(0) != 7) {
            fail("list.moveNode(list2)) failed");
        }
        
        list1.moveNode(list2);
        System.err.println("list after list.moveNode(list2): " + list1);
        System.err.println("Residual list list2 is: " + list2 + "\n");
        if (list1.getNth(0) != 13) {
            fail("list.moveNode(list2)) failed");
        }
        
        try {
            list1.moveNode(null);
        } catch (NullPointerException e) {
            System.err.println("After list.moveNode(null): " + e + " was thrown");
        }
    }
    
    @Test
    public final void testPurge() {
        LinkedList<Integer> list = new LinkedListImpl<Integer>(17);
        list.append(1);
        list.append(5);
        list.append(7);
        list.append(13);
        list.append(10);
        list.append(-1);
        list.append(17);
        list.append(1);
        list.append(5);
        list.append(13);
        list.append(17);
        list.append(5);
        list.append(-1);
        list.append(17);
        list.append(17);
        System.err.println("At init() " + list + "\n");
        
        list.purge();
        System.err.println("After purge() " + list);
        if (list.size() != 0) {
            fail("testpurge() failed");
        }
    }
    
    @Test
    public final void testRecursiveReverse() {
        LinkedListImpl<Integer> list = new LinkedListImpl<Integer>();
        System.err.println("list: " + list);
        /* LinkedList result = */list.recursiveReverse();
        System.err.println("After recursiveReverse(): " + list + "\n");
        
        list = new LinkedListImpl<Integer>(1);
        System.err.println("list: " + list);
        /* result = */list.recursiveReverse();
        System.err.println("After recursiveReverse(): " + list + "\n");
        
        list = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(1).append(3);
        System.err.println("list: " + list);
        /* result = */list.recursiveReverse();
        System.err.println("After recursiveReverse(): " + list + "\n");
        
        list = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(1).append(3).append(7);
        System.err.println("list: " + list);
        /* result = */list.recursiveReverse();
        System.err.println("After recursiveReverse(): " + list + "\n");
    }
    
    @Test
    public final void testRemove() {
        LinkedList<Integer> list = new LinkedListImpl<Integer>(17);
        list.append(1);
        list.append(5);
        list.append(7);
        list.append(13);
        list.append(10);
        list.append(-1);
        list.append(17);
        list.append(1);
        list.append(5);
        list.append(13);
        list.append(17);
        list.append(5);
        list.append(-1);
        list.append(17);
        list.append(17);
        System.err.println("At init() " + list + "\n");
        
        list.remove(17);
        System.err.println("After remove(17)\n" + list);
        if ((list != null) && (list.countOccurence(17) != 0))
            fail();
        
        list.remove(100);
        System.err.println("After remove(100)\n" + list);
        if ((list != null) && (list.countOccurence(100) != 0))
            fail();
        
        list.remove(23);
        System.err.println("After remove(23)\n" + list);
        if ((list != null) && (list.countOccurence(23) != 0))
            fail();
        
        list.remove(12);
        System.err.println("After remove(12)\n" + list);
        if ((list != null) && (list.countOccurence(12) != 0))
            fail();
        
        list.remove(-1);
        System.err.println("After remove(-1)\n" + list);
        if ((list != null) && (list.countOccurence(-1) != 0))
            fail();
        
        list.remove(1);
        System.err.println("After remove(1)\n" + list);
        if ((list != null) && (list.countOccurence(1) != 0))
            fail();
        
        list.remove(5);
        System.err.println("After remove(5)\n" + list);
        if ((list != null) && (list.countOccurence(5) != 0))
            fail();
        
        list.remove(7);
        System.err.println("After remove(7)\n" + list);
        if ((list != null) && (list.countOccurence(7) != 0))
            fail();
        
        list.remove(13);
        System.err.println("After remove(13)\n" + list);
        if ((list != null) && (list.countOccurence(13) != 0))
            fail();
        
        list.remove(10);
        System.err.println("After remove(10)\n" + list);
        if ((list != null) && (list.countOccurence(10) != 0))
            fail();
    }
    
    @Test
    public final void testRemoveNode() {
        LinkedList<Integer> list = new LinkedListImpl<Integer>(17);
        list.append(1);
        list.append(5);
        list.append(7);
        list.append(13);
        list.append(10);
        list.append(-1);
        list.append(17);
        list.append(1);
        list.append(5);
        list.append(13);
        list.append(17);
        list.append(5);
        list.append(-1);
        list.append(17);
        list.append(23);
        System.err.println("At init: \n" + list);
        
        list.removeNode(0);
        System.err.println("After removeNode(0)\n" + list);
        if (list.getNth(0) == 17)
            fail();
        int lastNodeIndex = list.size() - 1;
        
        list.removeNode(lastNodeIndex);
        System.err.println("After removeNode(" + lastNodeIndex + ")\n" + list);
        if (list.getNth(lastNodeIndex - 1) == 23)
            fail();
        
        list.removeNode(3);
        System.err.println("After removeNode(3)\n" + list);
        if (list.getNth(3) != 10)
            fail();
        
        try {
            list.removeNode(-5);
        } catch (Exception e) {
            System.err.println("After removeNode(-5)\n" + list);
        }
        
        try {
            list.removeNode(500);
        } catch (Exception e) {
            System.err.println("After removeNode(500)\n" + list);
        }
    }
    
    @Test
    public final void testReverse() {
        LinkedList<Integer> list = new LinkedListImpl<Integer>(17).append(1).append(5).append(7).append(13).append(10).append(-1)
                .append(17).append(1).append(5).append(13).append(17).append(5).append(-1).append(17).append(21);
        System.err.println("Before reverse()\n" + list);
        list.reverse();
        System.err.println("After reverse()\n" + list);
        if (list.getNth(0) != 21) {
            fail();
        }
    }
    
    @Test
    public final void testSetAt() {
        LinkedList<Integer> list = new LinkedListImpl<Integer>(17).append(1).append(5).append(7).append(13).append(10).append(-1)
                .append(17).append(1).append(5).append(13).append(17).append(5).append(-1).append(17).append(17);
        System.err.println("At init() " + list + "\n");
        
        list.setAt(0, 21);
        System.err.println("After list.setAt(0, 21)\n" + list);
        if (list.getNth(0) != 21)
            fail();
        
        list.setAt(5, 35);
        System.err.println("After list.setAt(5, 35)\n" + list);
        if (list.getNth(5) != 35)
            fail();
        
        try {
            list.setAt(-1, 35);
        } catch (Exception e) {
            System.err.println("setAt(-1, 35) threw exception");
        }
        System.err.println("After list.setAt(-1, 35)\n" + list);
        
        try {
            list.setAt(10003, 35);
        } catch (Exception e) {
            System.err.println("setAt(10003, 35) threw exception");
        }
        System.err.println("After list.setAt(10003, 35)\n" + list);
    }
    
    @Test
    public final void testSortedIntersection() {
        LinkedListImpl<Integer> list1 = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(0).append(1).append(2).append(3).append(4)
                .append(5).append(6).append(7);
        System.out.println("List1: " + list1 + "\n list2: " + null);
        LinkedList<Integer> result = list1.sortedListIntersection(null);
        System.out.println("result: " + result + "\n");
        if (result != null)
            fail();
        
        LinkedListImpl<Integer> list2 = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(0).append(2).append(4).append(6);
        System.out.println("List1: " + list1 + "\n list2: " + list2);
        result = list1.sortedListIntersection(list2);
        System.out.println("result: " + result + "\n");
        if (result.getNth(0) != 0)
            fail();
        if (result.getNth(1) != 2)
            fail();
        if (result.getNth(2) != 4)
            fail();
        if (result.getNth(3) != 6)
            fail();
        
        list1 = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(0).append(1).append(2).append(3).append(4).append(5).append(6)
                .append(7);
        list2 = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(1).append(3).append(5).append(8).append(9);
        System.out.println("List1: " + list1 + "\n list2: " + list2);
        result = list1.sortedListIntersection(list2);
        System.out.println("result: " + result + "\n");
        if (result.getNth(0) != 1)
            fail();
        if (result.getNth(1) != 3)
            fail();
        if (result.getNth(2) != 5)
            fail();
        
        list1 = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(0).append(1).append(2).append(3).append(4).append(5).append(6)
                .append(7);
        list2 = (LinkedListImpl<Integer>) new LinkedListImpl<Integer>(10).append(23).append(55).append(68).append(79);
        System.out.println("List1: " + list1 + "\n list2: " + list2);
        result = list1.sortedListIntersection(list2);
        System.out.println("result: " + result + "\n");
        if (result != null)
            fail();
    }
    
    @Test
    public final void testSplit() {
        LinkedList<Integer> list = new LinkedListImpl<Integer>(17);
        list.append(1);
        list.append(5);
        list.append(7);
        list.append(13);
        list.append(10);
        list.append(-1);
        list.append(17);
        list.append(1);
        list.append(5);
        list.append(13);
        list.append(17);
        list.append(5);
        list.append(-1);
        list.append(17);
        list.append(17);
        list.append(12);
        list.append(15);
        System.err.println("At init() " + list + "\n");
        
        final int numOfSplits = 2;
        System.out.println("List to split into " + numOfSplits);
        try {
            LinkedListImpl<Integer>[] lists = (LinkedListImpl<Integer>[]) list.split(numOfSplits);
            for (int i = 0; i < numOfSplits; i++) {
                System.out.println("List " + i + " is " + lists[i]);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
}
